(function($) {
  "use strict"

  // Page Preloader
  $(window).load(function() {
    $(".loader").delay(300).fadeOut();
    $(".animationload").delay(600).fadeOut("slow");
  });

  function resizeHeader () {
    // top bar + nav bar + paddign * 2 + border * 2
    $('.header-outer-container').css('height', $('#topbar').height() + $("#header-style-1").height() + 24 * 2 + 1 * 2);
  }
  $(window).on('resize', resizeHeader);
  resizeHeader();

  // Lock header in fixed position
  $("#header-style-1").affix({
    offset: {
      top: function () {
        return $('#topbar').height() + 10;
      },
      bottom: function () {
        return (this.bottom = $('#copyrights').outerHeight(true))
      }
    }
  })
  // DM Top
  jQuery(window).scroll(function(){
    if (jQuery(this).scrollTop() > 1) {
      jQuery('.dmtop').css({bottom:"25px"});
    } else {
      jQuery('.dmtop').css({bottom:"-100px"});
    }
  });
  jQuery('.dmtop').click(function(){
    jQuery('html, body').animate({scrollTop: '0px'}, 500);
    return false;
  });


  // TOOLTIP
  $('.social-icons, .bs-example-tooltips, .footer-section, .social-icons').tooltip({
    selector: "[data-toggle=tooltip]",
    container: "body"
  });

  const $blogArchive = $('.blog-archive');

  $blogArchive.find('.year-link').on('click', function(e) {
    e.preventDefault();
    $(e.currentTarget).parent().find('ul').toggle();
  });


  new WOW().init();

  // Cookie consent
  function setCookieConsent() {
    try {
      localStorage.setItem('tcCoookieConsent', 'true');
    } catch (err) {
      console.log('Colud not store cookie consent confirmation', err);
    }
  }

  function getCookieConsent() {
    try {
      return localStorage.getItem('tcCoookieConsent');
    } catch (err) {
      console.log('Colud not get cookie consent', err);
      return false;
    }
  }

  if (!getCookieConsent()) {
    $('.cookie-consent-notice').show();
    $('.cookie-consent-notice-cancel').on('click', function() {
      setCookieConsent();
      $('.cookie-consent-notice').hide();
    });
  }

  // Newsletter popup
  function handleKeydown(e) {
    if (e.keyCode === 27) {
      hidePopup();
    }
  }

  function showPopup() {
    $('.newsletter-popup').show();

    setTimeout(function() {
      var formIframe = document.querySelector('#newsletter_popup_form');
      formIframe.src = formIframe.dataset.src;
    }, 200);

    document.addEventListener('keydown', handleKeydown);
  }

  function hidePopup() {
    document.removeEventListener('keydown', handleKeydown);
    $('.newsletter-popup').hide();
  }

  $('.newsletter-mobile-subscribe').on('click', function() {
    $('.newsletter-popup')
      .removeClass('mobile-form-closed')
      .addClass('mobile-form-open');
  });

  $('.newsletter-popup .close').on('click', function() {
    hidePopup();
  });

  /**
   * tcNewsletterTimer can have 3 states:
   * - undefined: Not yet set, probably first page load
   * - -1: Dialog already seen
   * - unix timestamp in ms: when the countdown started
   */

  function getFirstNewsletterLoad() {
    try {
      return parseInt(localStorage.getItem('tcNewsletterTimer'));
    } catch (err) {
      console.log('Colud not get newsletter timer', err);
      return 0;
    }
  }

  function setFirstNewsletterLoad(val) {
    try {
      localStorage.setItem('tcNewsletterTimer', val);
    } catch (err) {
      console.log('Colud not store newsletter timer', err);
    }
  }

  var newsletterLoad = getFirstNewsletterLoad();

  if (!newsletterLoad) {
    newsletterLoad = Date.now()
    setFirstNewsletterLoad(newsletterLoad);
  }

  if (newsletterLoad > 0) {
    // Show 45 sec after first load
    var msUntilShow = newsletterLoad + 1000 * 45 - Date.now();
    msUntilShow = Math.max(msUntilShow, 0);
    console.log('Showing newsletter dialog in', msUntilShow);

    setTimeout(function() {
      showPopup();
      setFirstNewsletterLoad(-1);
    }, msUntilShow);
  }

  function getLastLogin() {
    try {
      return parseInt(localStorage.getItem('tcLastLoggedIn'));
    } catch (err) {
      return 0;
    }
  }

  function setLastLogin(val) {
    try {
      localStorage.setItem('tcLastLoggedIn', val);
      console.log('tcLastLoggedIn set to ' + val);
    } catch (err) {
      console.log('Could not set tcLastLoggedIn to ' + val);
    }
  }

  $('[data-track-signin]').on('click', function() {
    setLastLogin(Date.now());
  });

  (function initShowEmails () {
    $.get('/geolocation', function (body) {
      if (body.primaryMarket) {
        // Show email addresses

        // Contact page
        $('[name="contact-phone"]').removeClass('col-sm-offset-3');
        $('[name="contact-emails"]').removeClass('hidden');
      }
    });
  })();

})(jQuery);



