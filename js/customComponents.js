function TCCarousel ($element, opts) {
  this.opts = opts || {};
  this.opts.loop = this.opts.loop || false;

  this.$element = $element;
  var self = this;
  $element.find('[data-carousel-link]').on('click', function (e) {
    e.preventDefault();
    var $a = $(this);

    self.navigate($a.data('carousel-link'));
  });

  $element.find('.navigate-forward').on('click', function () { self.navigateRelative(1); });
  $element.find('.navigate-back').on('click', function () { self.navigateRelative(-1); });

  this.counter = 1;
}

TCCarousel.prototype.navigateRelative = function (direction) {
  var stage;
  if (direction === -1) {
    stage = this.$element.find('[data-carousel="' + this.stage + '"]').prev().data('carousel');

    if (!stage && this.opts.loop) {
      stage = this.$element.find('[data-carousel]').last().data('carousel');
    }
  } else if (direction === 1) {
    stage = this.$element.find('[data-carousel="' + this.stage + '"]').next().data('carousel');

    if (!stage && this.opts.loop) {
      stage = this.$element.find('[data-carousel]').first().data('carousel');
    }
  }

  if (stage) {
    this.navigate(stage);
  }
};

TCCarousel.prototype.navigate = function (stage) {
  var lastStage = this.stage;
  this.stage = stage;

  this.$element.find('[data-carousel]').addClass('carousel-hidden').removeClass('carousel-active');

  this.$element.find('[data-carousel="' + stage + '"]')
    .removeClass('carousel-hidden')
    .css('z-index', this.counter++)
    .addClass('carousel-active');

  this.$element.find('[data-carousel-link]').removeClass('active');
  this.$element.find('[data-carousel-link="' + stage + '"]').addClass('active');
};

TCCarousel.prototype.toggleArrows = function (enabled) {
  this.$element.find('.navigate-forward').toggle(enabled);
  this.$element.find('.navigate-back').toggle(enabled);
};

TCCarousel.prototype.stopTimer = function () {
  clearTimeout(this.timer);
};

TCCarousel.prototype.startTimer = function (enabled) {
  var self = this;
  this.stopTimer();

  this.timer = setInterval(function () {
    self.navigateRelative(1);
  }, 10000);
}

TCCarousel.init = function ($element, first) {
  var headerCarousel = new TCCarousel($element, { loop: true });
  headerCarousel.navigate(first);

  function calculateSize () {
    var isMobile = window.matchMedia && window.matchMedia("only screen and (max-width: 760px)");

    if (isMobile && isMobile.matches) {
      headerCarousel.toggleArrows(false);
      headerCarousel.startTimer();
    } else {
      headerCarousel.stopTimer();
      headerCarousel.toggleArrows(true);
    }
  }


  calculateSize();

  window.addEventListener('resize', calculateSize);

  return headerCarousel;
};


function ImageSlideshow ($element) {
  this.$element = $element;

  this.$images = $element.find('.image-slideshow-images img, .image-slideshow-images [data-full-text]');
  this.$texts = $element.find('.image-slideshow-text > div');

  this.count = 0;
  this.go(0);
}

ImageSlideshow.prototype.go = function (index, backwards) {
  if (this.animationInProgress) {
    return;
  }

  this.showImage(this.index, index, backwards);
  this.showText(this.index, index);
  this.index = index; 

  var isSpecial = this.$images[index] && $(this.$images[index]).data('full-text') !== undefined;
  
  this.$element.toggleClass('full-text', isSpecial);
};

ImageSlideshow.prototype.showText = function (oldIndex, newIndex) {
    $(this.$texts[oldIndex]).hide();
    $(this.$texts[newIndex]).show();
};

ImageSlideshow.prototype.showImage = function (oldIndex, newIndex, backwards) {
  var self = this;
  var oldIndex = this.index;
  this.count++;
  this.animationInProgress = true;

  setTimeout(function () {
    $(self.$images[oldIndex]).removeClass('slideshow-visible from-right');
    self.animationInProgress = false;
  }, 200);

  if (backwards) {
    $(this.$images[newIndex]).addClass('from-right')
  }

  setTimeout(function () {
    $(self.$images[newIndex]).addClass('slideshow-visible').css('z-index', self.count);
  }, 50);

};

ImageSlideshow.isMobile = function () {
  return window.matchMedia && window.matchMedia("only screen and (max-width: 760px)").matches;
};

ImageSlideshow.prototype.getNextIndex = function (currentIndex) {
  if (currentIndex === undefined) {
    currentIndex = this.index;
  }
  var index;
  if (currentIndex >= this.$images.length - 1) {
    index = 0;
  } else {
    index = currentIndex + 1;
  }

  if (ImageSlideshow.isMobile() && $(this.$images[index]).data('desktop-only') !== undefined) {
    return this.getNextIndex(index);
  }

  return index;
};

ImageSlideshow.prototype.getPreviousIndex = function (currentIndex) {
  if (currentIndex === undefined) {
    currentIndex = this.index;
  }
  var index;

  if (currentIndex > 0) {
    index = currentIndex - 1;
  } else {
    index = this.$images.length - 1;
  }

  if (ImageSlideshow.isMobile() && $(this.$images[index]).data('desktop-only') !== undefined) {
    return this.getPreviousIndex(index);
  }

  return index;
};

ImageSlideshow.prototype.next = function () {
  this.go(this.getNextIndex());
};

ImageSlideshow.prototype.previous = function () {
  this.go(this.getPreviousIndex(), true);
};

ImageSlideshow.init = function ($slideshow) {
  var slideshow = new ImageSlideshow($slideshow);

  $slideshow.find('[name="slideshow_next"]').on('click', slideshow.next.bind(slideshow));
  $slideshow.find('[name="slideshow_prev"]').on('click', slideshow.previous.bind(slideshow));
};
